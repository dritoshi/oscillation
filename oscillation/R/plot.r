#' Draw double plot from clocklab(TM) data
#'
#' This function read binary data of clocklab (TM).
#'
#' @usage make.double.plot.data(data, shift.hour)
#' @param data clocklab data
#' @param shift.hour
#' @return A matrix
#' @export
#' @examples
#' file <- file.path(
#'   "/Users/itoshi/Projects/dev/R/oscillation",
#'   "oscillation",
#'   "inst",
#'   "data",
#'   "clocklab.dat"
#' )
#' data <- read.clocklab(file)
#' shift.hour <- 7  # shift 7 hours
#' data.dp <- make.double.plot.data(data, shift.hour)
make.double.plot.data <- function(data, shift.hour) {

  leftPadding  <- as.numeric(format(data$date[1],                 "%H")) # hour of start
  rightPadding <- 24 - as.numeric(format(data$date[length(data$date)], "%H")) - 1 # hour of start

  # minitus, 60 min * 24 h = 1440 min
  single.data <- matrix(
	c(
	  rep(0, leftPadding  * 60),  # start point is zero
      as.vector(data$data),
	  rep(0, rightPadding * 60)   # start point is zero
    ),
    ncol = 24 * 60,
    byrow = T
  )

  # shift
  single.data.vec <- as.vector(t(single.data))

   # min
  left.start <- 1
  left.end   <- shift.hour * 60
  right.end <- length(as.vector(single.data))
  right.start <- right.end - (24 - shift.hour) * 60 + 1

  shift.single.data.vec <- single.data.vec[-c(left.start:left.end, right.start:right.end)]
  shift.single.data <- matrix(shift.single.data.vec, ncol = 24 * 60, byrow = T)

  b <- rbind( shift.single.data[2:dim(shift.single.data)[1],], rep(0, 60 * 24) )
  double.data <- cbind(shift.single.data, b)
  #return(single.data)

}

#' Draw double plot from clocklab(TM) data
#'
#' This function read binary data of clocklab (TM).
#'
#' @usage double.plot(data, light.on, light.off, ...)
#' @param data clocklab data
#' @param light.on a hour of turning light on
#' @param light.off a hour of turning light off
#' @param ... parameter for color2D.matplot
#' @return dev.new()
#' @export
#' @examples
#' file <- file.path(
#'   "/Users/itoshi/Projects/dev/R/oscillation",
#'   "oscillation",
#'   "inst",
#'   "data",
#'   "clocklab.dat"
#' )
#' data <- read.clocklab(file)
#'
#' pdf.file <- file.path(
#'   "/Users/itoshi/Projects/dev/R/oscillation",
#'   "oscillation",
#'   "inst",
#'   "data",
#'   "clocklab.doubleplot.pdf"
#' )
#' pdf(pdf.file)
#' double.plot(data, light.on = 7, light.off = 19, main = "clocklab")
#' dev.off()
double.plot <- function(data, light.on, light.off, ...) {
	
  double.data <- make.double.plot.data(data, light.on)

  # double plot
  double.cellcol <- double.data
  double.cellcol[double.cellcol == 1] <- "#000000"
  double.cellcol[double.cellcol == 0] <- "#FFFFFF"

#  times      <- seq(from = 0, to = 48, by = 4) * 60  
#  timelabels <- c(rep(seq(from = 0, to = 20, by = 4), 2), 0)

  times      <- seq(from = 0, to = 48, by = 4) * 60
  timelabels <- c(rep(seq(from = 0, to = 20, by = 4), 2), 0) + light.on

  timelabels[which(timelabels >= 24)] <- timelabels[which(timelabels >= 24)] - 24

  allsec <- dim(data$data)[2]
  obs.range <- as.numeric(data$dates[allsec] - data$dates[1])
  step.axis <- 3

  # why do you subtract 10*60*60? becouse, started observation time is 10 o'clock.
  # daylabels <- data$dates[1] + seq(from=1, to=obs.range, by=step.axis)*60*24*step.axis - 10*60*60
  # unit is sec.
  daylabels <- data$dates[1] + seq(from = 0, to = obs.range, by = step.axis) * 60 * 24 * 60 - 10 * 60 * 60
  daylabels <- format(daylabels, "%y%m%d")
  days      <- obs.range - seq(from = 0, to = obs.range, by = step.axis)

  lights.on  <- 24 * 60  # c(single plot, double plot)
  lights.off <- c(light.off - light.on, light.off - light.on + 24) * 60

  par(las=1)
  color2D.matplot(double.data,
	 cellcolors = double.cellcol, axes = F,
     xlab = "Hours",
     ylab = "",
     border = NA,
     ...
  )
  axis(1, times, timelabels, col.axis = 'black')
  axis(2, days,  daylabels,  col.axis = 'black')
  abline(v = lights.on,  lty = 2)
  abline(v = lights.off, lty = 2)

}
