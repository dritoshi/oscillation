#' Transform clocklab data to TS object
#'
#' This function transforms clocklab(TM) data to TS object
#'
#' @usage clocklab2ts(data, type, ...)
#' @param data clocklab data
#' @param type byday or byhour
#' @param ... parameters of ts()
#' @return A TS object
#' @export
#' @details
#' If you indicates "byhour", counts of mean in a hour were returned.
#' @examples
#' file <- file.path(
#'   "/Users/itoshi/Projects/dev/R/oscillation",
#'   "oscillation",
#'   "inst",
#'   "data",
#'   "clocklab.dat"
#' )
#' data    <- read.clocklab(file)
#' data.ts <- clocklab2ts(data, type = "byhour", start = 1, frequency = 1)
clocklab2ts <- function(data, type, ...) {
  if (type == "byhour") {
    return( ts(colMeans(data$data),  ...) )	
  }
  if (type == "byday") {
    return( ts(as.vector(data$data), ...) )
  }
  if (is.na(type)) {
    warn(msg = "Error: byday or byhour")
  }
}

#' Transform clocklab data to TS object
#'
#' This function transforms clocklab(TM) data to TS object
#'
#' @usage plot.spectrum(x, ...)
#' @param x ts
#' @param ... parameters of spectrum()
#' @return A numeric vector of predicted period
#' @export
#' @examples
#' file <- file.path(
#'   "/Users/itoshi/Projects/dev/R/oscillation",
#'   "oscillation",
#'   "inst",
#'   "data",
#'   "clocklab.dat"
#' )
#' data    <- read.clocklab(file)
#' data.ts <- clocklab2ts(data, type = "byhour", start = 1, frequency = 24)
#' pdf.file <- file.path(
#'   "/Users/itoshi/Projects/dev/R/oscillation",
#'   "oscillation",
#'   "inst",
#'   "data",
#'   "clocklab.spec.pgram.pdf"
#' )
#' pdf(pdf.file)
#' plot.spectrum(data.ts, method = "pgram", spans = c(3,3), xlim = c(0,3))
#' plot.spectrum(data.ts, method = "ar",    xlim = c(0,3))
#' dev.off()
plot.spectrum <- function(x, ...) {
  #x.pg <- spec.pgram(x, ...)
  x.pg <- spectrum(x, ...)
  
  max.spec <- max(x.pg$spec) 
  max.freq <- x.pg$freq[max.spec == x.pg$spec]
  predicted.period <- frequency(x) * 1 / max.freq
  
  text(max.freq * 1.1, max.spec, round(predicted.period, 1))

  return(c(predicted.period, max.spec, max.freq))
}

#' Plotting spectrum of activity and estimation of period
#'
#' This function plots spectrum of activity and estimates of period
#'
#' @usage plot.spec.ar(x, freq.limit, ...)
#' @param x ts
#' @param freq.limit a vector (start, end)
#' @param ... parameters of spec.ar()
#' @return A numeric vector of predicted period
#' @export
#' @examples
#' file <- file.path(
#'   "/Users/itoshi/Projects/dev/R/oscillation",
#'   "oscillation",
#'   "inst",
#'   "data",
#'   "clocklab.dat"
#' )
#' data    <- read.clocklab(file)
#' data.ts <- clocklab2ts(data, type = "byhour", start = 1, frequency = 24)
#' pdf.file <- file.path(
#'   "/Users/itoshi/Projects/dev/R/oscillation",
#'   "oscillation",
#'   "inst",
#'   "data",
#'   "clocklab.spec.ar.pdf"
#' )
#' pdf(pdf.file)
#' plot.spec.ar(data.ts, freq.limit = c(8, 34), method = "burg", xlim = c(0,3))
#' dev.off()
plot.spec.ar <- function(x, freq.limit, ...) {
  x.pg <- spec.ar(x, ...)
  
  freq.limit.start <- freq.limit[1]
  freq.limit.end   <- freq.limit[2]  
  max.spec <- max(x.pg$spec[freq.limit.start:freq.limit.end])
  max.freq <- x.pg$freq[max.spec == x.pg$spec]
  predicted.period <- frequency(x) * 1 / max.freq
  
  text(max.freq * 1.1, max.spec, round(predicted.period, 1))

  return(c(predicted.period, max.spec, max.freq))
}


#' Omit clocklab data
#'
#' This function omits clocklab(TM) data
#'
#' @usage omitClockLabData(data, start.lt)
#' @param data     clocklab data
#' @param start.lt starting data point for omitting POSIXlt
#' @return A TS object
#' @export
#' @details
#' If you indicates "byhour", counts of mean in a hour were returned.
#' @examples
#' file <- file.path(
#'   "/Users/itoshi/Projects/dev/R/oscillation",
#'   "oscillation",
#'   "inst",
#'   "data",
#'   "clocklab.dat"
#' )
#' data    <- read.clocklab(file)
#' data.ts <- clocklab2ts(data, type = "byhour", start = 1, frequency = 1)
#' 
#' start.lt <- as.POSIXlt("2010-01-06 07:00:00")
#' new.data.ts <- omitClockLabData(data, start.lt)
omitClockLabData <- function(data, start.lt) {

  start.index <- grep(start.lt, data$dates)
  max.index   <- length(data$dates)

  cat(start.index, max.index, "\n")

  new.data <- data
  new.data$dates    <- data$dates[start.index:max.index]
  new.data$channels <- data$channels[start.index:max.index]
  new.data$data     <- data$data[,start.index:max.index]
  new.data$lights   <- data$lights[,start.index:max.index]
  new.data$hour_count <- max.index - start.index

  return(new.data)
}
