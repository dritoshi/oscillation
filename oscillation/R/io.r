#' Read data of clocklab format
#'
#' This function read binary data of clocklab (TM).
#'
#' @usage read.clocklab(file, omb_filter = TRUE)
#' @param file clocklab file
#' @param omb_filter TRUE or FALSE
#' @return A list
#' @export
#' @details
#' Data format is ...
#' @examples
#' file <- file.path(
#'   "/Users/itoshi/Projects/dev/R/oscillation",
#'   "oscillation",
#'   "inst",
#'   "data",
#'   "clocklab.dat"
#' )
#' clocklab <- read.clocklab(file)
read.clocklab <- function (file, omb_filter = TRUE) {

  if ( !file.exists(file) ) {
    stop("File '", file, "' does not exist.")
  }
  
  if ( file.access(file, 4) ) {
     stop("No read permission for file ", file)
  }
  
  con <- file(file, "rb") # Read in binary mode
  on.exit(close(con)) # closing the pipe on exit of this function

  file.length <- readBin(con, integer(), n = 1, size = 4, endian = "big")  # 0-3 bytes
  hour_count  <- file.length / 172

  data     <- matrix(0, ncol = hour_count, nrow = 60)
  lights   <- matrix(0, ncol = hour_count, nrow = 60)
  datestrs <- rep("", hour_count)
  hours    <- rep(0,  hour_count)
  channels <- rep(integer(0),  hour_count)
  seconds  <- rep(integer(0), hour_count)

  # read one record (172 bytes)
  for (i in 1:hour_count){
    readBin(con, integer(), n = 28, size = 1) #skipping the first 28 bytes in the record (0-27)
    datestrs[i] <- rawToChar(readBin(con, "raw", 10))
    # dates are strings in "mm/dd/yyyy" fashion
    # using "readBin" makes bunch of errors
    seconds[i]  <- readBin(con, integer(), n = 1,  size = 4, endian = "big")
    hours[i]    <- readBin(con, integer(), n = 1,  size = 1, endian = "little")
    channels[i] <- readBin(con, integer(), n = 1,  size = 1, endian = "little")
                   readBin(con, integer(), n = 4,  size = 1) # not use
    data[,i]    <- readBin(con, integer(), n = 60, size = 1, endian = "little")
                   readBin(con, integer(), n = 4,  size = 1) # not use
    lights[,i]  <- readBin(con, integer(), n = 60, size = 1, endian = "little")
  }

  # convert text date data to POSIXlt
  x <- paste(datestrs, hours) # making "%m/%d/%Y %H" style text
  dates <- strptime(x, "%m/%d/%Y %H") #conversion to POSIXlt

  # apply one-minute-boolean filter if needed
  if (omb_filter){
    data <- ifelse(data > 0, 1, 0)  # the boolean filter
  }

  return( list(hour_count = hour_count,
               data       = data,
               channels   = channels,
               lights     = lights,
               dates      = dates,
               filename   = file,
               omb_filter = omb_filter)
         )
  # hour_count = how many hours?
  # data = matrix of bytes with hour_count x 60 dimension. Each cells
  # counts the activity for one minitue.
  # dates = array of POSIXlt for each hours.
  # filename = file
  
}
