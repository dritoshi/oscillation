file <- file.path(
  "/Users/itoshi/Projects/dev/R/oscillation",
  "oscillation",
  "inst",
  "data",
  "clocklab.dat"
)

data <- read.clocklab(file)
# data.dp <- make.double.plot.data(data)
data.ts <- clocklab2ts(data, type = "byhour", start = 1, frequency = 24)

pdf("test.pdf")
# double plot
double.plot(data)
# spec. pgram.
predicted.period <- plot.spec.pgram(data.ts, spans = c(3,3), xlim = c(0,3))
dev.off()
